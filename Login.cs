using System;
using libreriaAlgoritmos;
using libreriaAlgoritmos.AccesoDatos;
using libreriaAlgoritmos.Entidades;
using proyectoAlgoritmos.Algoritmos;

namespace proyectoAlgoritmos
{
    internal class LoginMensajes
    {
        internal const string estadoInvalido = "\nEl estado ingresado no existe.";
        internal const string sucursalInexistente = "\nLa sucursal no existe.";

    }

    internal class Login
    {
        private AlgoritmosSucursales algoritmosSucursales = new AlgoritmosSucursales();
        private ExpresionesRegulares expresionesRegulares = new ExpresionesRegulares();
        private Adiciones adiciones = new Adiciones();
        private readonly SucursalAccesoDatos sucursalAccesoDatos = new SucursalAccesoDatos();

        public void Ingresar()
        {
            var menuAdministrador = new MenuAdministrador();
            var menuUsuario = new MenuUsuario();
            int opcionUsuario;
            bool sucursalValida = false;
            string estadoUsuario, sucursalUsuario, contrasena;
            Sucursal sucursalEncontrada;
            var listaSucursales = sucursalAccesoDatos.ObtenerListaTabla();

            algoritmosSucursales.QuicksortSucursales(listaSucursales, 0, listaSucursales.Count - 1);

            do
            {
                Console.Write("Ingresa el estado: ");
                estadoUsuario = Console.ReadLine();

                Console.Write("Ingresa la sucursal: ");
                sucursalUsuario = Console.ReadLine();

                sucursalEncontrada = algoritmosSucursales.BusquedaBinaria(listaSucursales, 0, listaSucursales.Count - 1, sucursalUsuario);

                if (sucursalEncontrada != null)
                {
                    if (sucursalEncontrada.NombreEstado != estadoUsuario && sucursalEncontrada.NombreSucursal == sucursalUsuario)
                        Console.WriteLine(LoginMensajes.estadoInvalido);
                    else
                        sucursalValida = true;
                }
                else
                    Console.WriteLine(LoginMensajes.sucursalInexistente);
            }
            while (sucursalValida == false);

            do
            {
                Console.WriteLine("\n1. Administrador.");
                Console.WriteLine("2. Usuario.");

                opcionUsuario = expresionesRegulares.ValidarEnteros("Opcion: ");
            }
            while (opcionUsuario < 1 || opcionUsuario > 2);

            switch(opcionUsuario)
            {
                case 1:
                    do
                    {
                        Console.Write("\nContrasena: ");
                        contrasena = adiciones.OcultarTexto();
                    }
                    while (contrasena.Length == 0 || contrasena != "12345");
                    
                    menuAdministrador.Menu(sucursalEncontrada);
                    break;
                case 2:
                    menuUsuario.Menu(sucursalEncontrada);
                    break;
            }
        }
    }
}