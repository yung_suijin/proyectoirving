using System;
using libreriaAlgoritmos.AccesoDatos;
using libreriaAlgoritmos.Entidades;
using proyectoAlgoritmos.Algoritmos;

namespace proyectoAlgoritmos.Gestores
{
    internal class GestorPeliculasMensajes
    {
        internal const string peliculaNoEncontrada = "\nLa pelicula ingresada no existe.";
        internal const string peliculaDuplicada = "\nLa pelicula ya existe y no puede ser duplicada.";
    }

    internal class GestorPeliculas
    {
        private int opcionUsuario;
        private string nombrePelicula;
        private Pelicula peliculaEncontrada;
        private Pelicula pelicula = new Pelicula();
        private AlgoritmosPeliculas algoritmosPeliculas = new AlgoritmosPeliculas();
        private readonly PeliculaAccesoDatos peliculaAccesoDatos = new PeliculaAccesoDatos();

        public void NuevaPelicula()
        {
            do
            {
                var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();
                
                algoritmosPeliculas.QuicksortPeliculas(listaPeliculas, 0, listaPeliculas.Count - 1);

                do
                {
                    Console.Write("\nNombre: ");
                    pelicula.NombrePelicula = Console.ReadLine();

                    peliculaEncontrada = algoritmosPeliculas.BusquedaBinaria(listaPeliculas, 0, listaPeliculas.Count - 1, pelicula.NombrePelicula);

                    if (peliculaEncontrada != null)
                        Console.WriteLine(GestorPeliculasMensajes.peliculaDuplicada);
                }
                while (peliculaEncontrada != null);

                IngresarVariables();

                if (peliculaAccesoDatos.NuevaPelicula(pelicula) == false)
                    Console.WriteLine(Mensajes.baseDeDatosSinActualizar);
                else
                {
                    Console.WriteLine(Mensajes.baseDeDatosActualizada);
                    Console.Write("\nIngresar una nueva pelicula? (0 / 1): ");

                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public void Eliminar()
        {
            do
            {
                var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();
                
                algoritmosPeliculas.QuicksortPeliculas(listaPeliculas, 0, listaPeliculas.Count - 1);

                do
                {
                    Console.Write("\nNombre de la pelicula a eliminar: ");
                    nombrePelicula = Console.ReadLine();

                    peliculaEncontrada = algoritmosPeliculas.BusquedaBinaria(listaPeliculas, 0, listaPeliculas.Count - 1, nombrePelicula);

                    if (peliculaEncontrada == null)
                        Console.WriteLine(GestorPeliculasMensajes.peliculaNoEncontrada);
                }
                while (peliculaEncontrada == null);

                if (peliculaAccesoDatos.EliminarPelicula(nombrePelicula) == false)
                    Console.WriteLine(Mensajes.baseDeDatosSinActualizar);
                else
                {
                    Console.WriteLine(Mensajes.baseDeDatosActualizada);
                    Console.Write("\nEliminar otra pelicula? (0 / 1): ");

                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public void Modificar()
        {
            do
            {
                var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();
                
                algoritmosPeliculas.QuicksortPeliculas(listaPeliculas, 0, listaPeliculas.Count - 1);

                do
                {
                    Console.Write("\nNombre de la pelicula a modificar: ");
                    nombrePelicula = Console.ReadLine();

                    peliculaEncontrada = algoritmosPeliculas.BusquedaBinaria(listaPeliculas, 0, listaPeliculas.Count - 1, nombrePelicula);

                    if (peliculaEncontrada == null)
                        Console.WriteLine(GestorPeliculasMensajes.peliculaNoEncontrada);
                }
                while (peliculaEncontrada == null);

                pelicula.NombrePelicula = nombrePelicula;
                IngresarVariables();

                if (peliculaAccesoDatos.ModificarPelicula(pelicula) == false)
                    Console.WriteLine(Mensajes.baseDeDatosSinActualizar);
                else
                {
                    Console.WriteLine(Mensajes.baseDeDatosActualizada);
                    Console.Write("\nModificar otra pelicula? (0 / 1): ");
                    
                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public void Listar()
        {
            string nombre = "Nombre", director = "Director", productor = "Productor", clasificacion = "Clasificacion", 
                duracion = "Duracion", genero = "Genero";

            var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();
            algoritmosPeliculas.QuicksortPeliculas(listaPeliculas, 0, listaPeliculas.Count - 1);

            Console.WriteLine($"\n{nombre.PadRight(50)}{director.PadRight(20)}{productor.PadRight(20)}{clasificacion.PadRight(15)}{duracion.PadRight(10)}" +
                $"{genero.PadRight(15)}\n");

            foreach (var info in listaPeliculas)
            {
                string duracionLista = info.Duracion.ToString();
                string duracionDisplay = duracionLista + " min";

                Console.WriteLine(info.NombrePelicula.PadRight(50) +
                    info.Director.PadRight(20) +
                    info.Productor.PadRight(20) +
                    info.Clasificacion.PadRight(15) +
                    duracionDisplay.PadRight(10) +
                    info.Genero.PadRight(15));
            }
        }

        private void IngresarVariables()
        {
            var expresionesRegulares = new ExpresionesRegulares();
            string duracion;

            Console.Write("Director: ");
            pelicula.Director = Console.ReadLine();
            Console.Write("Productor: ");
            pelicula.Productor = Console.ReadLine();
            Console.Write("Clasificacion: ");
            pelicula.Clasificacion = Console.ReadLine();

            do
            {
                Console.Write("Duracion: ");
                duracion = Console.ReadLine();
            }
            while (expresionesRegulares.ValidarDatosNumericos(duracion) == false);

            pelicula.Duracion = Int32.Parse(duracion);
            Console.Write("Genero: ");
            pelicula.Genero = Console.ReadLine();
        }
    }
}