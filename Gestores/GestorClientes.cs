using System;
using System.Collections.Generic;
using System.Linq;
using libreriaAlgoritmos;
using libreriaAlgoritmos.AccesoDatos;
using libreriaAlgoritmos.Entidades;

namespace proyectoAlgoritmos.Gestores
{
    internal class GestorClientesMensajes
    {
        internal const string buclePelicula = "\nBuscar otra pelicula? (0 / 1): ";
        internal const string bucleGenero = "\nBuscar otro genero? (0 / 1): ";
        internal const string bucleClasificacion = "\nBuscar otra clasificacion? (0 / 1): ";
    }
    
    internal class GestorClientes
    {
        private int opcionUsuario;
        private AlgoritmosFunciones algoritmosFunciones = new AlgoritmosFunciones();
        private FuncionesQueryable funcionesQueryable = new FuncionesQueryable();
        private readonly FuncionAccesoDatos funcionAccesoDatos = new FuncionAccesoDatos();
        private readonly PeliculaAccesoDatos peliculaAccesoDatos = new PeliculaAccesoDatos();
        
        public void BuscarPeliculaNombre(Sucursal sucursalIngreso)
        {
            string nombrePelicula;
            var cola = new Queue<Cartelera>();
            var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
            
            algoritmosFunciones.QuicksortFunciones(listaHorarios, 0, listaHorarios.Count - 1);

            var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();
            var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();

            do
            {
                Console.Write("\nNombre: ");
                nombrePelicula = Console.ReadLine();

                var listaCartelera = funcionesQueryable.InfoFuncionesPelicula(listaFuncionesSucursal, listaPeliculas, nombrePelicula, sucursalIngreso.NombreSucursal).ToList();

                if (listaCartelera.Count == 0)
                    Console.WriteLine("\nLa pelicula buscada no existe o no tiene funciones.");
                else
                {
                    ListaDisplay();

                    listaCartelera.ForEach(l => ImprimirDatos(l));

                    Console.Write(GestorClientesMensajes.buclePelicula);

                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public void BuscarPeliculaClasificacion(Sucursal sucursalIngreso)
        {
            string clasificacionPelicula;
            var cola = new Queue<Cartelera>();
            var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
            
            algoritmosFunciones.QuicksortFunciones(listaHorarios, 0, listaHorarios.Count - 1);

            var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();
            var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();

            do
            {
                Console.Write("\nClasificacion: ");
                clasificacionPelicula = Console.ReadLine();

                var listaCartelera = funcionesQueryable.InfoFuncionesClasificacion(listaFuncionesSucursal, listaPeliculas, clasificacionPelicula, sucursalIngreso.NombreSucursal).ToList();

                if (listaCartelera.Count == 0)
                    Console.WriteLine("\nLa clasificacion buscada no existe o no tiene funciones.");
                else
                {
                    ListaDisplay();

                    listaCartelera.ForEach(l => ImprimirDatos(l));

                    Console.Write(GestorClientesMensajes.bucleClasificacion);

                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public void BuscarPeliculaGenero(Sucursal sucursalIngreso)
        {
            string genero;
            var cola = new Queue<Cartelera>();
            var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
            
            algoritmosFunciones.QuicksortFunciones(listaHorarios, 0, listaHorarios.Count - 1);

            var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();
            var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();

            do
            {
                Console.Write("\nGenero: ");
                genero = Console.ReadLine();

                var listaCartelera = funcionesQueryable.InfoFuncionesGenero(listaFuncionesSucursal, listaPeliculas, genero, sucursalIngreso.NombreSucursal).ToList();

                if (listaCartelera.Count == 0)
                    Console.WriteLine("\nEl genero buscado no existe o no tiene funciones.");
                else
                {
                    ListaDisplay();

                    listaCartelera.ForEach(l => ImprimirDatos(l));

                    Console.Write(GestorClientesMensajes.bucleGenero);

                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public string OrdenarCartelera()
        {
            string opcion;

            do
            {
                Console.Write("\n(A / D): ");
                opcion = Console.ReadLine();
            }
            while (!opcion.Equals("A") && !opcion.Equals("D"));

            return opcion;
        }

        public void ConsultarCartelera(Sucursal sucursalIngreso, string opcion)
        {
            string pelicula = "Pelicula", fecha = "Fecha", horaInicio = "Hora de inicio", horaFin = "Hora de finalizacion", sala = "Sala";
            var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
            var pilaCartelera = new Stack<Funcion>();
            
            var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();
            algoritmosFunciones.Seleccion(listaFuncionesSucursal, listaFuncionesSucursal.Count, 0);

            switch(opcion)
            {
                case "A":
                    Console.WriteLine($"\n{pelicula.PadRight(50)}{fecha.PadRight(15)}{horaInicio.PadRight(20)}{horaFin.PadRight(25)}{sala.PadRight(8)}\n");

                    listaFuncionesSucursal.ForEach(l => CarteleraDisplay(l));
                    break;
                case "D":
                    Console.WriteLine($"\n{pelicula.PadRight(50)}{fecha.PadRight(15)}{horaInicio.PadRight(20)}{horaFin.PadRight(25)}{sala.PadRight(8)}\n");

                    foreach (var item in listaFuncionesSucursal)
                        pilaCartelera.Push(item);

                    while (pilaCartelera.Count > 0)
                    {
                        var funcion = pilaCartelera.Pop();
                        CarteleraDisplay(funcion);
                    }
                    break;
            }
        }

        private void ImprimirDatos(Cartelera cartelera)
        {
            string duracionLista = cartelera.Duracion.ToString();
            string duracionDisplay = duracionLista + " min";

            Console.WriteLine(cartelera.NombrePelicula.PadRight(35) +
                cartelera.Fecha.PadRight(15) +
                cartelera.IDSala.ToString().PadRight(8) +
                cartelera.HorarioInicio.PadRight(20) +
                cartelera.HorarioFinal.PadRight(25) +
                cartelera.Productor.PadRight(25) +
                cartelera.Director.PadRight(25) +
                duracionDisplay.PadRight(15));
        }

        private void ListaDisplay()
        {
            string pelicula = "Pelicula", fecha = "Fecha", sala = "Sala", horaInicio = "Hora de inicio", horaFin = "Hora de finalizacion",
                productor = "Productor", director = "Director", duracion = "Duracion";

            Console.WriteLine($"\n{pelicula.PadRight(35)}{fecha.PadRight(15)}{sala.PadRight(8)}{horaInicio.PadRight(20)}{horaFin.PadRight(25)}" +
                    $"{productor.PadRight(25)}{director.PadRight(25)}{duracion.PadRight(15)}");
        }

        private void CarteleraDisplay(Funcion funcion)
        {
            Console.WriteLine(funcion.NombrePelicula.PadRight(50) +
                funcion.Fecha.PadRight(15) +
                funcion.HorarioInicio.PadRight(20) +
                funcion.HorarioFinal.PadRight(25) +
                funcion.IDSala.ToString().PadRight(8));
        }
    }
}