using System;
using System.Collections.Generic;
using System.Linq;
using libreriaAlgoritmos;
using libreriaAlgoritmos.AccesoDatos;
using libreriaAlgoritmos.Entidades;
using proyectoAlgoritmos.Algoritmos;

namespace proyectoAlgoritmos.Gestores
{
    internal class GestorFuncionesMensajes
    {
        internal const string funcionNoEncontrada = "\nLa funcion para la pelicula ingresada no existe.";
        internal const string salaNoEncontrada = "\nLa sala ingresada no existe.";
    }

    internal class GestorFunciones
    {
        private int opcionUsuario, idSala;
        private string nombrePelicula, horarioUsuario, fechaUsuario;
        private int[] arregloSalas;
        private Salas sala = new Salas();
        private AlgoritmosFunciones algoritmosFunciones = new AlgoritmosFunciones();
        private FuncionesQueryable funcionesQueryable = new FuncionesQueryable();
        private ExpresionesRegulares expresionesRegulares = new ExpresionesRegulares();
        private readonly FuncionAccesoDatos funcionAccesoDatos = new FuncionAccesoDatos();
        
        public void NuevoHorario(Sucursal sucursalIngreso)
        {
            int salaEncontrada, contadorFunciones;
            TimeSpan horarioUsuarioTiempo;
            TimeSpan? duracionFinal;
            var horarioApertura = TimeSpan.Parse("08:00");
            var horarioCierre = TimeSpan.Parse("23:30");
            var peliculaAccesoDatos = new PeliculaAccesoDatos();
            var algoritmosPeliculas = new AlgoritmosPeliculas();
            Pelicula peliculaEncontrada;
            var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
            var listaPeliculas = peliculaAccesoDatos.ObtenerListaTabla();
            arregloSalas = sala.SalasCine();

            algoritmosPeliculas.QuicksortPeliculas(listaPeliculas, 0, listaPeliculas.Count - 1);

            do
            {
                do
                {
                    Console.Write("\nPelicula: ");
                    nombrePelicula = Console.ReadLine();

                    peliculaEncontrada = algoritmosPeliculas.BusquedaBinaria(listaPeliculas, 0, listaPeliculas.Count - 1, nombrePelicula);

                    if (peliculaEncontrada == null)
                        Console.WriteLine(GestorPeliculasMensajes.peliculaNoEncontrada);
                }
                while (peliculaEncontrada == null);

                contadorFunciones = funcionAccesoDatos.ContadorFuncionesMaximas(peliculaEncontrada.NombrePelicula, sucursalIngreso.NombreSucursal);

                if (contadorFunciones >= 10)
                    Console.WriteLine("\nLa pelicula ingresada ya supera el maximo de horarios (10) permitidos para la sucursal.");
            }
            while (contadorFunciones >= 10);

            do
            {
                idSala = expresionesRegulares.ValidarEnteros("Sala: ");

                salaEncontrada = algoritmosFunciones.BusquedaBinariaSala(arregloSalas, 0, arregloSalas.Length - 1, idSala);

                if (salaEncontrada == -1)
                    Console.WriteLine(GestorFuncionesMensajes.salaNoEncontrada);
            }
            while (salaEncontrada == -1);

            do
            {
                Console.Write("Fecha (mm/dd/yy): ");
                fechaUsuario = Console.ReadLine();
            }
            while (expresionesRegulares.ValidarFormatoFecha(fechaUsuario) == false);

            var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();
            var listaFuncionesSala = funcionAccesoDatos.TablaArbolFuncionesSala(listaFuncionesSucursal, idSala).ToList();

            algoritmosFunciones.QuicksortFunciones(listaFuncionesSala, 0, listaFuncionesSala.Count - 1);

            do
            {
                do
                {
                    do
                    {
                        Console.Write("Hora de inicio: ");
                        horarioUsuario = Console.ReadLine();
                    }
                    while (expresionesRegulares.ValidarFormatoHora(horarioUsuario) == false);

                    horarioUsuarioTiempo = TimeSpan.Parse(horarioUsuario);

                    if (horarioUsuarioTiempo <= horarioApertura || horarioUsuarioTiempo >= horarioCierre)
                        Console.WriteLine("\nEl horario no puede ser registrado a la hora de apertura/cierre del cine.");
                }
                while (horarioUsuarioTiempo <= horarioApertura || horarioUsuarioTiempo >= horarioCierre);

                int duracionHoras = (peliculaEncontrada.Duracion - peliculaEncontrada.Duracion % 60) / 60;
                int duracionMinutos = (peliculaEncontrada.Duracion - duracionHoras * 60);
                var listaFuncionesFecha = funcionesQueryable.FuncionesFecha(listaFuncionesSala, fechaUsuario).ToList();

                duracionFinal = algoritmosFunciones.Rango(listaFuncionesFecha, horarioUsuario, duracionHoras, duracionMinutos);

                if (duracionFinal != null)
                {
                    var nuevaFuncion = new Funcion()
                    {
                        IDSala = idSala,
                        IDPelicula = peliculaEncontrada.IDPelicula,
                        HorarioInicio = horarioUsuario,
                        HorarioFinal = duracionFinal.Value.ToString("hh\\:mm"),
                        NombrePelicula = peliculaEncontrada.NombrePelicula,
                        NombreSucursal = sucursalIngreso.NombreSucursal,
                        Fecha = fechaUsuario
                    };

                    if (funcionAccesoDatos.NuevaFuncion(nuevaFuncion) == false)
                        Console.WriteLine(Mensajes.baseDeDatosSinActualizar);
                    else
                        Console.WriteLine(Mensajes.baseDeDatosActualizada);
                }
            }
            while (duracionFinal == null);
        }

        public void EliminarHorario(Sucursal sucursalIngreso)
        {
            var funcionEncontrada = new List<Funcion>();
            arregloSalas = sala.SalasCine();

            do
            {
                var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
                var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();

                algoritmosFunciones.QuicksortFunciones(listaHorarios, 0, listaHorarios.Count - 1);

                do
                {
                    Console.Write("\nPelicula: ");
                    nombrePelicula = Console.ReadLine();

                    idSala = expresionesRegulares.ValidarEnteros("Sala: ");

                    Console.Write("Horario de inicio: ");
                    horarioUsuario = Console.ReadLine();

                    Console.Write("Fecha (mm/dd/yy): ");
                    fechaUsuario = Console.ReadLine();

                    funcionEncontrada = funcionesQueryable.FuncionEncontrada(listaFuncionesSucursal, nombrePelicula, idSala, horarioUsuario, fechaUsuario).ToList();

                    if (funcionEncontrada.Count == 0)
                        Console.WriteLine(GestorFuncionesMensajes.funcionNoEncontrada);
                }
                while (funcionEncontrada.Count == 0);

                string pelicula = funcionEncontrada.FirstOrDefault().NombrePelicula;
                string horarioInicio = funcionEncontrada.FirstOrDefault().HorarioInicio;
                string fecha = funcionEncontrada.FirstOrDefault().Fecha;
                int sala = funcionEncontrada.FirstOrDefault().IDSala;

                if (funcionAccesoDatos.EliminarFuncion(pelicula, horarioInicio, fecha, sala) == false)
                    Console.WriteLine(Mensajes.baseDeDatosSinActualizar);
                else
                {
                    Console.WriteLine(Mensajes.baseDeDatosActualizada);
                    Console.Write("\nEliminar otro horario? (0 / 1): ");

                    opcionUsuario = Int32.Parse(Console.ReadLine());
                }
            }
            while (opcionUsuario == 1);
        }

        public void CarteleraSucursal(Sucursal sucursalIngreso)
        {
            string pelicula = "Pelicula", horaInicio = "Hora de inicio", horaFin = "Hora de finalizacion", sala = "Sala", 
                sucursal = "Sucursal", fecha = "Fecha";
            var listaHorarios = funcionAccesoDatos.ObtenerListaTabla();
            var listaFuncionesSucursal = funcionAccesoDatos.HorariosFuncionesSucursal(listaHorarios, sucursalIngreso.NombreSucursal).ToList();

            algoritmosFunciones.Seleccion(listaFuncionesSucursal, listaFuncionesSucursal.Count, 0);

            Console.WriteLine($"\n{pelicula.PadRight(50)}{horaInicio.PadRight(20)}{horaFin.PadRight(25)}{sala.PadRight(8)}{sucursal.PadRight(15)}{fecha.PadRight(10)}\n");

            foreach (var info in listaFuncionesSucursal)
            {
                Console.WriteLine(info.NombrePelicula.PadRight(50) +
                    info.HorarioInicio.PadRight(20) +
                    info.HorarioFinal.PadRight(25) +
                    info.IDSala.ToString().PadRight(8) +
                    info.NombreSucursal.PadRight(15) +
                    info.Fecha.PadRight(10));
            }
        }
    }
}