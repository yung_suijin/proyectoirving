using System;
using libreriaAlgoritmos.Entidades;
using proyectoAlgoritmos.Gestores;

namespace proyectoAlgoritmos
{
    internal class MenuAdministrador
    {
        private ExpresionesRegulares expresionesRegulares = new ExpresionesRegulares();
        private readonly GestorPeliculas gestorPeliculas = new GestorPeliculas();
        private readonly GestorFunciones gestorFunciones = new GestorFunciones();

        public void Menu(Sucursal sucursalIngreso)
        {
            int opcionUsuario;

            do
            {
                Console.WriteLine("\n1. Nueva pelicula.");
                Console.WriteLine("2. Ingresar horario.");
                Console.WriteLine("3. Eliminar pelicula.");
                Console.WriteLine("4. Eliminar horario.");
                Console.WriteLine("5. Modificar pelicula.");
                Console.WriteLine("6. Consultar peliculas.");
                Console.WriteLine("7. Consultar cartelera sucursal.");
                Console.WriteLine("8. Salir.");

                opcionUsuario = expresionesRegulares.ValidarEnteros("Opcion: ");

                switch (opcionUsuario)
                {
                    case 1:
                        gestorPeliculas.NuevaPelicula();
                        break;
                    case 2:
                        gestorFunciones.NuevoHorario(sucursalIngreso);
                        break;
                    case 3:
                        gestorPeliculas.Eliminar();
                        break;
                    case 4:
                        gestorFunciones.EliminarHorario(sucursalIngreso);
                        break;
                    case 5:
                        gestorPeliculas.Modificar();
                        break;
                    case 6:
                        gestorPeliculas.Listar();
                        break;
                    case 7:
                        gestorFunciones.CarteleraSucursal(sucursalIngreso);
                        break;
                    case 8:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("\nLa opcion ingresada es invalida.");
                        break;
                }
            }
            while (opcionUsuario != 8);
        }
    }
}