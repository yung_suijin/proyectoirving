namespace proyectoAlgoritmos
{
    internal class Mensajes
    {
        internal const string baseDeDatosSinActualizar = "\nLa base de datos no pudo actualizarse";
        internal const string baseDeDatosActualizada = "\nLa base de datos se ha actualizado";
    }
}