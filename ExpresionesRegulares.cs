using System;
using System.Text.RegularExpressions;

namespace proyectoAlgoritmos
{
    internal class ExpresionesRegulares
    {
        public int ValidarEnteros(string mensaje)
        {
            int dato;
            string numero;

            do
            {
                Console.Write(mensaje);
                numero = Console.ReadLine();

                Int32.TryParse(numero, out dato);
            }
            while (dato == 0);

            return dato;
        }

        public bool ValidarDatosNumericos(string inputUsuario)
        {
            var rg = new Regex(@"^[0-9\s,]*$");

            if(rg.IsMatch(inputUsuario) == false)
                Console.WriteLine("\nNo se permiten caracteres especiales o numeros negativos.");

            return rg.IsMatch(inputUsuario);
        }

        public bool ValidarFormatoHora(string inputUsuario)
        {
            var rg = new Regex(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");

            if(rg.IsMatch(inputUsuario) == false)
                Console.WriteLine("\nEl formato de hora ingresado no es valido.");

            return rg.IsMatch(inputUsuario);
        }

        public bool ValidarFormatoFecha(string inputUsuario)
        {
            var rg = new Regex(@"^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$");

            if(rg.IsMatch(inputUsuario) == false)
                Console.WriteLine("\nEl formato de fecha ingresado no es valido.");

            return rg.IsMatch(inputUsuario);
        }
    }
}