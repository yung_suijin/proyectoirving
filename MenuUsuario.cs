using System;
using libreriaAlgoritmos.Entidades;
using proyectoAlgoritmos.Gestores;

namespace proyectoAlgoritmos
{
    internal class MenuUsuario
    {
        private string inputUsuario;
        private ExpresionesRegulares expresionesRegulares = new ExpresionesRegulares();
        private readonly GestorClientes gestorClientes = new GestorClientes();

        public void Menu(Sucursal sucursalIngreso)
        {
            int opcionUsuario;

            do
            {
                Console.WriteLine("\n1. Buscar pelicula por nombre.");
                Console.WriteLine("2. Buscar pelicula por clasificacion.");
                Console.WriteLine("3. Buscar pelicula por genero.");
                Console.WriteLine("4. Ordenar Cartelera (A o D).");
                Console.WriteLine("5. Consultar Cartelera.");
                Console.WriteLine("6. Salir.");

                opcionUsuario = expresionesRegulares.ValidarEnteros("Opcion: ");

                switch (opcionUsuario)
                {
                    case 1:
                        gestorClientes.BuscarPeliculaNombre(sucursalIngreso);
                        break;
                    case 2:
                        gestorClientes.BuscarPeliculaClasificacion(sucursalIngreso);
                        break;
                    case 3:
                        gestorClientes.BuscarPeliculaGenero(sucursalIngreso);
                        break;
                    case 4:
                        inputUsuario = gestorClientes.OrdenarCartelera();
                        Menu(sucursalIngreso);
                        break;
                    case 5:
                        if (inputUsuario != null)
                            gestorClientes.ConsultarCartelera(sucursalIngreso, inputUsuario);
                        else
                            Console.WriteLine("\nElige una opcion de ordenamiento antes de consultar la cartelera.");
                        break;
                    case 6:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("\nLa opcion ingresada es invalida.");
                        break;
                }
            }
            while (opcionUsuario != 6);
        }
    }
}