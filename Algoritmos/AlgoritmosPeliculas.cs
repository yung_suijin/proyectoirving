using System.Collections.Generic;
using libreriaAlgoritmos.Entidades;

namespace proyectoAlgoritmos.Algoritmos
{
    public class AlgoritmosPeliculas
    {
        public void QuicksortPeliculas(List<Pelicula> lista, int primero, int ultimo)
        {
            int i = primero;
            int j = ultimo;

            if (ultimo - primero >= 1)
            {
                string pivote = lista[primero].NombrePelicula;

                while (j > i)
                {
                    while (lista[i].NombrePelicula.CompareTo(pivote) <= 0 && i <= ultimo && j > i)
                        i++;
                    while (lista[j].NombrePelicula.CompareTo(pivote) > 0 && j >= primero && j >= i)
                        j--;

                    if (j > i)
                        IntercambiarPeliculas(lista, i, j);
                }

                IntercambiarPeliculas(lista, primero, j);
                QuicksortPeliculas(lista, primero, j - 1);
                QuicksortPeliculas(lista, j + 1, ultimo);
            }
            else { return; }
        }

        private void IntercambiarPeliculas(List<Pelicula> lista, int i, int j)
        {
            Pelicula temp = lista[i];
            lista[i] = lista[j];
            lista[j] = temp;
        }

        public Pelicula BusquedaBinaria(List<Pelicula> lista, int izquierda, int derecha, string inputUsuario)
        {
            int resultado = 0;

            while (izquierda <= derecha)
            {
                int puntoMedio = izquierda + (derecha - izquierda) / 2;

                resultado = inputUsuario.CompareTo(lista[puntoMedio].NombrePelicula);

                if (resultado == 0)
                {
                    var pelicula = new Pelicula()
                    {
                        IDPelicula = lista[puntoMedio].IDPelicula,
                        NombrePelicula = lista[puntoMedio].NombrePelicula,
                        Director = lista[puntoMedio].Director,
                        Productor = lista[puntoMedio].Productor,
                        Clasificacion = lista[puntoMedio].Clasificacion,
                        Duracion = lista[puntoMedio].Duracion,
                        Genero = lista[puntoMedio].Genero
                    };

                    return pelicula;
                }

                if (resultado > 0)
                    return BusquedaBinaria(lista, puntoMedio + 1, derecha, inputUsuario);

                else
                    return BusquedaBinaria(lista, izquierda, puntoMedio - 1, inputUsuario);
            }

            return null;
        }
    }
}