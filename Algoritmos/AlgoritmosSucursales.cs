using System.Collections.Generic;
using libreriaAlgoritmos.Entidades;

namespace proyectoAlgoritmos.Algoritmos
{
    internal class AlgoritmosSucursales
    {
        public void QuicksortSucursales(List<Sucursal> lista, int primero, int ultimo)
        {
            int i = primero;
            int j = ultimo;

            if (ultimo - primero >= 1)
            {
                string pivote = lista[primero].NombreSucursal;

                while (j > i)
                {
                    while (lista[i].NombreSucursal.CompareTo(pivote) <= 0 && i <= ultimo && j > i)
                        i++;
                    while (lista[j].NombreSucursal.CompareTo(pivote) > 0 && j >= primero && j >= i)
                        j--;

                    if (j > i)
                        IntercambiarSucursales(lista, i, j);
                }

                IntercambiarSucursales(lista, primero, j);
                QuicksortSucursales(lista, primero, j - 1);
                QuicksortSucursales(lista, j + 1, ultimo);
            }
            else { return; }
        }

        private void IntercambiarSucursales(List<Sucursal> lista, int i, int j)
        {
            Sucursal temp = lista[i];
            lista[i] = lista[j];
            lista[j] = temp;
        }

        public Sucursal BusquedaBinaria(List<Sucursal> lista, int izquierda, int derecha, string inputUsuario)
        {
            int resultado = 0;

            while (izquierda <= derecha)
            {
                int puntoMedio = izquierda + (derecha - izquierda) / 2;

                resultado = inputUsuario.CompareTo(lista[puntoMedio].NombreSucursal);

                if (resultado == 0)
                {
                    var sucursal = new Sucursal()
                    {
                        IDEstado = lista[puntoMedio].IDEstado,
                        IDSucursal = lista[puntoMedio].IDSucursal,
                        NombreEstado = lista[puntoMedio].NombreEstado,
                        NombreSucursal = lista[puntoMedio].NombreSucursal
                    };

                    return sucursal;
                }

                if (resultado > 0)
                    return BusquedaBinaria(lista, puntoMedio + 1, derecha, inputUsuario);

                else
                    return BusquedaBinaria(lista, izquierda, puntoMedio - 1, inputUsuario);
            }

            return null;
        }
    }
}