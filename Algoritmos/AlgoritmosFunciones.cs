using System;
using System.Collections.Generic;
using System.Linq;
using libreriaAlgoritmos.Entidades;

namespace proyectoAlgoritmos
{
    internal class AlgoritmosFunciones
    {
        public int BusquedaBinariaSala(int[] arregloSalas, int izquierda, int derecha, int sala)
        {
            if (derecha >= izquierda)
            {
                int puntoMedio = izquierda + (derecha - izquierda) / 2;

                if (arregloSalas[puntoMedio] == sala)
                    return puntoMedio;

                if (arregloSalas[puntoMedio] > sala)
                    return BusquedaBinariaSala(arregloSalas, izquierda, puntoMedio - 1, sala);

                return BusquedaBinariaSala(arregloSalas, puntoMedio + 1, derecha, sala);
            }

            return -1;
        }

        public TimeSpan? Rango(List<Funcion> listaFuncionesSala, string horario, int duracionHoras, int duracionMinutos)
        {
            var contador = 0;
            TimeSpan funcionInicioTiempo = new TimeSpan();
            TimeSpan? horaFinalFuncion = null;
            var horarioUsuario = TimeSpan.Parse(horario);
            var cola = new Queue<Funcion>();

            foreach (var item in listaFuncionesSala)
                cola.Enqueue(item);

            if (cola.Count == 0)
                horaFinalFuncion = CalcularFinalFuncion(horario, duracionHoras, duracionMinutos);
            else
            {
                while (cola.Count > 0)
                {
                    var funcion = cola.Dequeue();

                    var funcionInicio = TimeSpan.Parse(funcion.HorarioInicio);
                    var funcionFinal = TimeSpan.Parse(funcion.HorarioFinal);

                    if (horarioUsuario >= funcionInicio && horarioUsuario <= funcionFinal)
                    {
                        Console.WriteLine("\nNo se puede registrar el horario porque ya hay una funcion en ese intervalo de tiempo.");
                        return null;
                    }
                    else if (horarioUsuario < funcionInicio)
                    {
                        if (contador == 0)
                        {
                            funcionInicioTiempo = funcionInicio - new TimeSpan(0, 0, 30, 0);
                            horaFinalFuncion = CalcularFinalFuncion(horario, duracionHoras, duracionMinutos);

                            if (horarioUsuario < funcionInicioTiempo && horaFinalFuncion < funcionInicioTiempo)
                                return horaFinalFuncion;
                            else
                            {
                                Console.WriteLine("\nNo se puede registrar el horario porque la pelicula seleccionada coincide con el horario de tolerancia.");
                                return null;
                            }
                        }
                        else
                        {
                            var horarioToleranciaUltimaFuncion = TimeSpan.Parse(listaFuncionesSala[contador - 1].HorarioFinal) + new TimeSpan(0, 0, 30, 0);

                            funcionInicioTiempo = funcionInicio - new TimeSpan(0, 0, 30, 0);
                            horaFinalFuncion = CalcularFinalFuncion(horario, duracionHoras, duracionMinutos);

                            if (horarioUsuario > horarioToleranciaUltimaFuncion && horaFinalFuncion < funcionInicioTiempo)
                                return horaFinalFuncion;
                            else
                            {
                                Console.WriteLine("\nNo se puede registrar el horario porque la pelicula seleccionada coincide con el horario de tolerancia.");
                                return null;
                            }
                        }
                    }
                    else if (cola.Count == 0)
                    {
                        var funcionFinalTiempo = funcionFinal + new TimeSpan(0, 0, 30, 0);

                        if (horarioUsuario > funcionFinalTiempo)
                        {
                            horaFinalFuncion = CalcularFinalFuncion(horario, duracionHoras, duracionMinutos);
                        }
                        else
                        {
                            Console.WriteLine("\nNo se puede registrar el horario porque la pelicula seleccionada coincide con el horario de tolerancia de la ultima funcion.");
                        }
                    }

                    contador++;
                }
            }

            return horaFinalFuncion;
        }

        private TimeSpan CalcularFinalFuncion(string horario, int duracionHoras, int duracionMinutos)
        {
            int[] arregloPilaHorasUsuario, arregloPilaMinutosUsuario;
            var pila1 = new Stack<int>();
            var pila3 = new Stack<int>();
            var pila2 = new Stack<int>();
            var pila4 = new Stack<int>();

            char[] horasUsuario = horario.Split(':')[0].ToCharArray();
            char[] minutosUsuario = horario.Split(':')[1].ToCharArray();

            arregloPilaHorasUsuario = Array.ConvertAll(horasUsuario, h => (int)Char.GetNumericValue(h));
            arregloPilaMinutosUsuario = Array.ConvertAll(minutosUsuario, m => (int)Char.GetNumericValue(m));

            for (int i = 0; i < arregloPilaHorasUsuario.Length; i++)
                pila1.Push(arregloPilaHorasUsuario[i]);

            for (int i = 0; i < arregloPilaMinutosUsuario.Length; i++)
                pila3.Push(arregloPilaMinutosUsuario[i]);

            int[] arregloHorasDuracion = Array.ConvertAll(duracionHoras.ToString().ToArray(), d => (int)d - 48);
            int[] arregloMinutosDuracion = Array.ConvertAll(duracionMinutos.ToString().ToArray(), d => (int)d - 48);

            for (int i = 0; i < arregloHorasDuracion.Length; i++)
                pila2.Push(arregloHorasDuracion[i]);

            for (int i = 0; i < arregloMinutosDuracion.Length; i++)
                pila4.Push(arregloMinutosDuracion[i]);

            var finalFuncion = TimeSpan.Parse(SumaHorario(pila1, pila2, pila3, pila4));

            return finalFuncion;
        }

        public string SumaHorario(Stack<int> pila1, Stack<int> pila2, Stack<int> pila3, Stack<int> pila4)
        {
            int suma, acarreo = 0, valor = 0;
            var pila5 = new Stack<int>();
            var pila6 = new Stack<int>();

            while (pila3.Count != 0 || pila4.Count != 0)
            {
                if (pila3.Count > pila4.Count)
                {
                    valor = Convert.ToInt32(pila3.Pop());
                    pila6.Push(valor);
                }
                else if (pila3.Count < pila4.Count)
                {
                    valor = Convert.ToInt32(pila4.Pop());
                    pila6.Push(valor);
                }
                else
                {
                    if (pila3.Count - 1 == 0 && pila4.Count - 1 == 0)
                    {
                        suma = Convert.ToInt32(pila3.Pop()) + Convert.ToInt32(pila4.Pop()) + acarreo;

                        if (suma >= 6)
                        {
                            suma -= 6;
                            acarreo = 1;
                            pila6.Push(suma);
                        }
                        else
                        {
                            acarreo = 0;
                            pila6.Push(suma);
                        }
                    }
                    else
                    {
                        suma = Convert.ToInt32(pila3.Pop()) + Convert.ToInt32(pila4.Pop()) + acarreo;

                        if (suma >= 10)
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila6.Push(suma);
                        }
                        else
                        {
                            acarreo = 0;
                            pila6.Push(suma);
                        }
                    }
                }
            }

            while(pila1.Count != 0 || pila2.Count != 0)
            {
                if (pila2.Count == 0)
                {
                    suma = Convert.ToInt32(pila1.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 0;
                        acarreo = 1;
                        pila5.Push(suma);
                        pila5.Push(acarreo);
                    }
                    else
                    {
                        acarreo = 0;
                        pila5.Push(suma);
                    }
                }
                else if (pila1.Count == 0)
                {
                    suma = Convert.ToInt32(pila2.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        suma -= 0;
                        acarreo = 1;
                        pila5.Push(suma);
                        pila5.Push(acarreo);
                    }
                    else
                    {
                        acarreo = 0;
                        pila5.Push(suma);
                    }
                }
                else
                {
                    suma = Convert.ToInt32(pila1.Pop()) + Convert.ToInt32(pila2.Pop()) + acarreo;

                    if (suma >= 10)
                    {
                        if(pila1.Count == 0 && pila2.Count == 0)
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila5.Push(suma);
                            pila5.Push(acarreo);
                        }
                        else
                        {
                            suma -= 10;
                            acarreo = 1;
                            pila5.Push(suma);
                        }
                    }
                    else
                    {
                        acarreo = 0;
                        pila5.Push(suma);
                    }
                }
            }

            string horas = string.Join("", pila5);
            int horasEntero = Int32.Parse(horas);

            if(horasEntero >= 24)
            {
                horasEntero -= 24;
                horas = "0" + horasEntero;
            }

            string minutos = string.Join("", pila6);
            string duracion = horas + ":" + minutos;

            return duracion;
        }

        public void QuicksortFunciones(List<Funcion> lista, int primero, int ultimo)
        {
            int i = primero;
            int j = ultimo;

            if (ultimo - primero >= 1)
            {
                string pivote = lista[primero].HorarioInicio;

                while (j > i)
                {
                    while (lista[i].HorarioInicio.CompareTo(pivote) <= 0 && i <= ultimo && j > i)
                        i++;
                    while (lista[j].HorarioInicio.CompareTo(pivote) > 0 && j >= primero && j >= i)
                        j--;

                    if (j > i)
                        IntercambiarFunciones(lista, i, j);
                }

                IntercambiarFunciones(lista, primero, j);
                QuicksortFunciones(lista, primero, j - 1);
                QuicksortFunciones(lista, j + 1, ultimo);
            }
            else { return; }
        }

        private void IntercambiarFunciones(List<Funcion> lista, int i, int j)
        {
            Funcion temp = lista[i];
            lista[i] = lista[j];
            lista[j] = temp;
        }

        public void Seleccion(List<Funcion> lista, int n, int index)
        {
            if (index == n)
                return;

            int k = Index(lista, index, n - 1);

            if (k != index)
            {
                Funcion temp = lista[k];
                lista[k] = lista[index];
                lista[index] = temp;
            }

            Seleccion(lista, n, index + 1);
        }

        private int Index(List<Funcion> lista, int i, int j)
        {
            var funcion = new Funcion();

            if (i == j)
                return i;

            int k = Index(lista, i + 1, j);

            return (lista[i].CompareTo(lista[k]) < 0) ? i : k;
        }
    }
}